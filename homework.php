<?php 
require 'php/db.php';
session_start();
if( empty($_SESSION['username'])): 
		header( "location: index.html" );
	endif;
?>
<!DOCTYPE html>
<html lang="hu">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>DigitalClassmate - Házi feladatok</title>
  <!-- Bootstrap core CSS-->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="loggedin.php">DigitalClassmate</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Kezdőlap">
          <a class="nav-link" href="loggedin.php">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Kezdőlap</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Házi feladatok">
          <a class="nav-link" href="homework.php">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Házi feladatok</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dolgozatok">
          <a class="nav-link" href="tests.php">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">Dolgozatok</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Órarend">
          <a class="nav-link" href="timetable.php">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Órarend</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Chatszoba">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-file"></i>
            <span class="nav-link-text">Chat-szoba</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseExamplePages">
            <li>
              <a href="globalchat.php">Globális</a>
            </li>
            <li>
              <a href="groupedchat.php">Csoportos</a>
            </li>
          </ul>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-envelope"></i>
            <span class="d-lg-none">Üzenetek
              
            </span>
            <span class="indicator text-primary d-none d-lg-block">
              <i class="fa fa-fw fa-circle"></i>
            </span>
          </a>
          <div class="dropdown-menu" aria-labelledby="messagesDropdown">
            <h6 class="dropdown-header">Új üzenetek:</h6>
            <div class="dropdown-divider"></div>
            <div class="dropdown-divider"></div>
            <div class="dropdown-divider"></div>
            <div class="dropdown-divider"></div>
          </div>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-bell"></i>
            <span class="d-lg-none">Riasztások

            </span>
            <span class="indicator text-warning d-none d-lg-block">
              <i class="fa fa-fw fa-circle"></i>
            </span>
          </a>
          <div class="dropdown-menu" aria-labelledby="alertsDropdown">
            <h6 class="dropdown-header">Új riasztások:</h6>
            <div class="dropdown-divider"></div>
            <div class="dropdown-divider"></div>
            <div class="dropdown-divider"></div>
            <div class="dropdown-divider"></div>
          </div>
        </li>
        <li class="nav-item">
          <form class="form-inline my-2 my-lg-0 mr-lg-2">
            <div class="input-group">
              <input class="form-control" type="text" placeholder="Keresés:">
              <span class="input-group-btn">
                <button class="btn btn-primary" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Kijelentkezés</a>
        </li>
      </ul>
    </div>
  </nav>
    
  <div class="content-wrapper">
      <div id="homeworknavigator">
        <a href="#irodalom"><button class="btn navigatorbtn">Irodalom</button></a>
        <a href="#nyelvtan"><button class="btn navigatorbtn">Nyelvtan</button></a>
        <a href="#matek"><button class="btn navigatorbtn">Matematika</button></a>
        <a href="#tori"><button class="btn navigatorbtn">Történelem</button></a>
        <a href="#angol"><button class="btn navigatorbtn">Angol</button></a>
        <a href="#biosz"><button class="btn navigatorbtn">Biológia</button></a>
        <a href="#halozat"><button class="btn navigatorbtn">Hálózat</button></a>
        <a href="#adatb"><button class="btn navigatorbtn">Adatbázis</button></a>
        <a href="#munkaszerv"><button class="btn navigatorbtn">Munkaszervezés</button></a>
    </div>
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <h1>Házi feladatok</h1>
      <hr>
      <div id="homeworksdiv">
		<div class="container-fluid homeworks" id="irodalom">
		  <!-- Breadcrumbs-->
		  <h2>Magyar Irodalom</h2>
		  <hr>
		  <div>
			<div class="homeworkmessagecontainer">
				
			</div>
			<div class="homeworkinputscontainer">
				<input type="text" placeholder="Házi:" id="irodalomhomeworkinput" class="form-control homewrokinputs"/>
				<button id="irodalomhomeworksenter" class="btn homeworkadd">Hozzáadás</button>
			</div>
		  </div>
		</div>
		<div class="container-fluid homeworks" id="nyelvtan">
		  <!-- Breadcrumbs-->
		  <h2>Magyar Nyelvtan</h2>
		  <hr>
		  <div>
			<div class="homeworkmessagecontainer">
				
			</div>
			<div class="homeworkinputscontainer">
				<input type="text" placeholder="Házi:" id="nyelvtanhomeworkinput" class="form-control homewrokinputs"/>
				<button id="nyelvtanhomeworksenter" class="btn homeworkadd">Hozzáadás</button>
			</div>
		  </div>
		</div>
		<div class="container-fluid homeworks" id="matek">
		  <!-- Breadcrumbs-->
		  <h2>Matematika</h2>
		  <hr>
		  <div>
			<div class="homeworkmessagecontainer">
				
			</div>
			<div class="homeworkinputscontainer">
				<input type="text" placeholder="Házi:" id="matekhomeworkinput" class="form-control homewrokinputs"/>
				<button id="matekhomeworksenter" class="btn homeworkadd">Hozzáadás</button>
			</div>
		  </div>
		</div>
		<div class="container-fluid homeworks" id="tori">
		  <!-- Breadcrumbs-->
		  <h2>Történelem</h2>
		  <hr>
		  <div>
			<div class="homeworkmessagecontainer">
				
			</div>
			<div class="homeworkinputscontainer">
				<input type="text" placeholder="Házi:" id="torihomeworkinput" class="form-control homewrokinputs"/>
				<button id="irodalomhomeworksenter" class="btn homeworkadd">Hozzáadás</button>
			</div>
		  </div>
		</div>
		<div class="container-fluid homeworks" id="angol">
		  <!-- Breadcrumbs-->
		  <h2>Angol</h2>
		  <hr>
		  <div>
			<div class="homeworkmessagecontainer">
				
			</div>
			<div class="homeworkinputscontainer">
				<input type="text" placeholder="Házi:" id="angolhomeworkinput" class="form-control homewrokinputs"/>
				<button id="angolhomeworksenter" class="btn homeworkadd">Hozzáadás</button>
			</div>
		  </div>
		</div>
		<div class="container-fluid homeworks" id="biosz">
		  <!-- Breadcrumbs-->
		  <h2>Biológia</h2>
		  <hr>
		  <div>
			<div class="homeworkmessagecontainer">
				
			</div>
			<div class="homeworkinputscontainer">
				<input type="text" placeholder="Házi:" id="biologiahomeworkinput" class="form-control homewrokinputs"/>
				<button id="biologiahomeworksenter" class="btn homeworkadd">Hozzáadás</button>
			</div>
		  </div>
		</div>
		<div class="container-fluid homeworks" id="halozat">
		  <!-- Breadcrumbs-->
		  <h2>Hálózat ismeret</h2>
		  <hr>
		  <div>
			<div class="homeworkmessagecontainer">
				
			</div>
			<div class="homeworkinputscontainer">
				<input type="text" placeholder="Házi:" id="halozathomeworkinput" class="form-control homewrokinputs"/>
				<button id="halozathomeworksenter" class="btn homeworkadd">Hozzáadás</button>
			</div>
		  </div>
		</div>
		<div class="container-fluid homeworks" id="adatb">
		  <!-- Breadcrumbs-->
		  <h2>Adatbázis és szoftverfejlesztés</h2>
		  <hr>
		  <div>
			<div class="homeworkmessagecontainer">
				
			</div>
			<div class="homeworkinputscontainer">
				<input type="text" placeholder="Házi:" id="adatbazishomeworkinput" class="form-control homewrokinputs"/>
				<button id="adatbazishomeworksenter" class="btn homeworkadd">Hozzáadás</button>
			</div>
		  </div>
		</div>
		<div class="container-fluid homeworks" id="munkaszerv">
		  <!-- Breadcrumbs-->
		  <h2>Munkaszervezés</h2>
		  <hr>
		  <div>
			<div class="homeworkmessagecontainer">
				
			</div>
			<div class="homeworkinputscontainer">
				<input type="text" placeholder="Házi:" id="munkaszervhomeworkinput" class="form-control homewrokinputs"/>
				<button id="munkaszervhomeworksenter" class="btn homeworkadd">Hozzáadás</button>
			</div>
		  </div>
		</div>
	  </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Biztosan ki akar jelentkezni?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Mégse</button>
            <a class="btn btn-primary" href="php/logout.php">Kijelentkezés</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <!-- Toggle between fixed and static navbar-->
    <script>
    $('#toggleNavPosition').click(function() {
      $('body').toggleClass('fixed-nav');
      $('nav').toggleClass('fixed-top static-top');
    });

    </script>
    <!-- Toggle between dark and light navbar-->
    <script>
    $('#toggleNavColor').click(function() {
      $('nav').toggleClass('navbar-dark navbar-light');
      $('nav').toggleClass('bg-dark bg-light');
      $('body').toggleClass('bg-dark bg-light');
    });

    </script>
  </div>
</body>

</html>
