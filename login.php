<?php 
require 'php/db.php';
session_start();
?>
<!DOCTYPE html>
<html lang="hu">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="WebProgrammer" >
    <title>Digital Classmate - Home</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <link href="css/agency.min.css" rel="stylesheet">
	<link href="css/loginstyle.css" rel="stylesheet">
  </head>
  <body>
<div id="container">
  <h1>Log In</h1>
    <span class="close-btn">
    <a href="index.html" id="closelink"><img src="https://cdn4.iconfinder.com/data/icons/miu/22/circle_close_delete_-128.png"></a>
      </span>

  <form action="php/access.php" method="post">
    <input type="text" name="username" placeholder="Username"/>
    <input type="password" name="password" placeholder="Password"/>
   <button type="submit" id="loginbutton" class="btn btn-primary" style="margin-left:20%;">Bejelentkezés</button>
    <div id="remember-container">
      <input type="checkbox" id="checkbox-2-1" class="checkbox" checked="checked"/>
      <span id="remember">Emlékezz rám</span>
      <span id="forgotten">Elfelejtett jelszó</span>
    </div>
</form>
</div>

<!-- Forgotten Password Container -->
      <div id="forgotten-container">
        <h1>Forgotten</h1>
          <span class="close-btn">
            <img src="https://cdn4.iconfinder.com/data/icons/miu/22/circle_close_delete_-128.png">
          </span>

        <form>
            <input type="email" name="email" placeholder="E-mail">
            <a href="#" class="orange-btn">Get new password</a>
        </form>
        <script>
                $('#login-button').click(function(){
              $('#login-button').fadeOut("slow",function(){
                $("#container").fadeIn();
                TweenMax.from("#container", .4, { scale: 0, ease:Sine.easeInOut});
                TweenMax.to("#container", .4, { scale: 1, ease:Sine.easeInOut});
              });
            });

            $(".close-btn").click(function(){
              TweenMax.from("#container", .4, { scale: 1, ease:Sine.easeInOut});
              TweenMax.to("#container", .4, { left:"0px", scale: 0, ease:Sine.easeInOut});
              $("#container, #forgotten-container").fadeOut(800, function(){
                $("#login-button").fadeIn(800);
              });
            });

            /* Forgotten Password */
            $('#forgotten').click(function(){
              $("#container").fadeOut(function(){
                $("#forgotten-container").fadeIn();
              });
            });
        </script>
      </div>
</body>
</html>