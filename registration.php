<?php 
require 'php/db.php';
session_start();
?>
<!DOCTYPE html>
<html lang="hu">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="WebProgrammer" >
    <title>Digital Classmate - Home</title>
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <link href="css/agency.min.css" rel="stylesheet">
	<link href="css/loginstyle.css" rel="stylesheet">
  </head>
  <body>
<div id="regcontainer">
  <h1>Regisztráció</h1>
    <span class="close-btn">
    <a href="index.html" id="closelink"><img src="https://cdn4.iconfinder.com/data/icons/miu/22/circle_close_delete_-128.png"></a>
      </span>

  <form action="php/signup.php" method="post">
    <input type="text" name="firstname" placeholder="Vezetéknév:">
    <input type="text" name="lastname" placeholder="Keresztnév:">
    <input type="text" name="username" placeholder="Felhasználónév:">
    <input type="email" name="email" placeholder="E-mail:">
    <input type="email" name="confemail" placeholder="E-mail újra:">
    <input type="password" name="password" placeholder="Jelszó:">
    <input type="password" name="confpassword" placeholder="Jelszó újra:">
      <label style="color:white; margin-left: 15%;">
          <strong>Osztály/Csoport:</strong>
      </label>
      <select>
        <option>12.E/1</option>
        <option>12.E/2</option>
        <option>Tanár</option>
      </select>
    <button type="submit" id="registrationbutton" class="btn btn-primary" style="margin-left:20%;">Regisztráció</button>
</form>
</div>
</body>
</html>