<?php
/* Displays all error messages */
session_start();
?>
<!DOCTYPE html>
<html>
<head>
  <title>Hiba</title>
    <link rel="stylesheet" href="phpstyle.css">
</head>
<body>
    <div id="header">
        <p>DigitalClassmate</p>
    </div>
<div class="form" id="uzenet">
    <h1>Hiba!</h1>
    <p>
    <?php 
    if( isset($_SESSION['message']) AND !empty($_SESSION['message']) ): 
        echo $_SESSION['message'];    
    else:
        header( "location: index.php" );
    endif;
    ?>
    </p>     
    <a href="index.php"><button id="homefromsucces" class="button button-block"/>Kezdőlap</button></a>
</div>
</body>
</html>
