<?php 

require 'db.php';
session_start();
$email = $_POST['email'];
$confemail = $_POST['confemail'];
$password = $_POST['password'];
$confpassword = $_POST['confpassword'];
if($email == $confemail){
    if($password == $confpassword){
            /* Registration process, inserts user info into the database 
				and sends account confirmation email message
			*/
			// Set session variables to be used on profile.php page
			$_SESSION['username'] = $_POST['username'];
			$_SESSION['email'] = $_POST['email'];
			$_SESSION['first_name'] = $_POST['firstname'];
			$_SESSION['last_name'] = $_POST['lastname'];
			$_SESSION['class'] = $_POST['class'];
			// Escape all $_POST variables to protect against SQL injections
			/*$username = $mysgli->escape_string($_POST['username']);
			$first_name = $mysqli->escape_string($_POST['firstname']);
			$last_name = $mysqli->escape_string($_POST['lastname']);
			$email = $mysqli->escape_string($_POST['email']);
			$password = $mysqli->escape_string(password_hash($_POST['password'], PASSWORD_BCRYPT));
			$hash = $mysqli->escape_string( md5( rand(0,1000) ) );*/
			$username =$_POST['username'];
			$first_name =$_POST['firstname'];
			$last_name =$_POST['lastname'];
			$class = $_POST['class'];
			$password = password_hash($_POST['password'], PASSWORD_BCRYPT);
			$hash= md5( rand(0,1000) );
			// Check if user with that email already exists
			$result = $mysqli->query("SELECT * FROM users WHERE email='$email'") or die($mysqli->error());

			// We know user email exists if the rows returned are more than 0
			if ( $result->num_rows > 0 ) {
				
				$_SESSION['message'] = 'Felhasználó evvel a névvel már létezik!';
				header("location: error.php");
				
			}
			else { // Email doesn't already exist in a database, proceed...

				// active is 0 by DEFAULT (no need to include it here)
				$sql = "INSERT INTO users (username, first_name, last_name, email, password, hash, class) " 
						. "VALUES ('$username','$first_name','$last_name','$email','$password', '$hash', '$class')";

				// Add user to the database
				if ( $mysqli->query($sql) ){

					$_SESSION['active'] = 0; //0 until user activates their account with verify.php
					$_SESSION['logged_in'] = true; // So we know the user has logged in
					$_SESSION['message'] =
							
							 "A megrősítő e-mailt küldtünk a $email címre, Kérjük
							 erősítse meg a regisztrációt!";

					// Send registration confirmation link (verify.php)
					$to      = $email;
					$subject = 'Jelszó megerősítése ( http://digitalclassmate.epizy.com )';
					$message_body = '
					Hello '.$first_name.',

					Köszönjük, hogy regisztrált!

					Kérjük kattintson a linkre a megerősítéshez:

					 http://digitalclassmate.epizy.com/php/verify.php?email='.$email.'&hash='.$hash;  

					mail( $to, $subject, $message_body );

					header("location: ../login.php"); 

				}

				else {
					$_SESSION['message'] = 'Registration failed!';
					header("location: error.php");
				}

				}
    }
    else {
        $_SESSION['message'] = "Nem egyeznek a jelszavak";
        header("location: error.php");
    }
}
else {
    $_SESSION['message'] = "Nem egyeznek az e-mail címek";
    header("location: error.php");
}
