<?php
/* User login process, checks if user exists and password is correct */

require 'db.php';
session_start();

$username = $_POST['username'];
$result = $mysqli->query("SELECT * FROM users WHERE username='$username'");

if ( $result->num_rows == 0 ){ // User doesn't exist
    $_SESSION['message'] = "Felhasználó ezzel a névvel nem létezik!";
    header("location:error.php");
}
else { // User exists
    $user = $result->fetch_assoc();

    if ( password_verify($_POST['password'], $user['password']) ) {
        
		$_SESSION['username'] = $user['username'];
        $_SESSION['email'] = $user['email'];
        $_SESSION['first_name'] = $user['first_name'];
        $_SESSION['last_name'] = $user['last_name'];
		$_SESSION['class'] = $user['class'];
        $_SESSION['active'] = $user['active'];
        
        // This is how we'll know the user is logged in
        $_SESSION['logged_in'] = true;

        header("location: ../loggedin.php");
    }
    else {
        $_SESSION['message'] = "Rossz jelszót adtál meg, probáld újra!";
        header("location: error.php");
    }
}

